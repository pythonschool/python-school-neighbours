Python School Neighbours Articles Plugin for Pelican
====================================

This plugin adds ``next_page`` (newer) and ``prev_page`` (older)
variables to the article's context.

Also adds ``next_page_in_category`` and ``prev_page_in_category``.


Usage
-----

.. code-block:: html+jinja

    <ul>
    {% if article.prev_page %}
        <li>
            <a href="{{ SITEURL }}/{{ article.prev_page.url}}">
                {{ article.prev_page.title }}
            </a>
        </li>
    {% endif %}
    {% if article.next_page %}
        <li>
            <a href="{{ SITEURL }}/{{ article.next_page.url}}">
                {{ article.next_page.title }}
            </a>
        </li>
    {% endif %}
   </ul>
   <ul>
    {% if article.prev_page_in_category %}
        <li>
            <a href="{{ SITEURL }}/{{ article.prev_page_in_category.url}}">
                {{ article.prev_page_in_category.title }}
            </a>
        </li>
    {% endif %}
    {% if article.next_page_in_category %}
        <li>
            <a href="{{ SITEURL }}/{{ article.next_page_in_category.url}}">
                {{ article.next_page_in_category.title }}
            </a>
        </li>
    {% endif %}
    </ul>

Usage with the Subcategory plugin
---------------------------------

If you want to get the neigbors within a subcategory it's a little different.
Since an article can belong to more than one subcategory, subcategories are
stored in a list. If you have an article with subcategories like

``Category/Foo/Bar``

it will belong to both subcategory Foo, and Foo/Bar. Subcategory neighbors are
added to an article as ``next_page_in_subcategory#`` and
``prev_page_in_subcategory#`` where ``#`` is the level of subcategory. So using
the example from above, subcategory1 will be Foo, and subcategory2 Foo/Bar.
Therefor the usage with subcategories is:

.. code-block:: html+jinja

    <ul>
    {% if article.prev_page_in_subcategory1 %}
        <li>
            <a href="{{ SITEURL }}/{{ article.prev_page_in_subcategory1.url}}">
                {{ article.prev_page_in_subcategory1.title }}
            </a>
        </li>
    {% endif %}
    {% if article.next_page_in_subcategory1 %}
        <li>
            <a href="{{ SITEURL }}/{{ article.next_page_in_subcategory1.url}}">
                {{ article.next_page_in_subcategory1.title }}
            </a>
        </li>
    {% endif %}
   </ul>
   <ul>
    {% if article.prev_page_in_subcategory2 %}
        <li>
            <a href="{{ SITEURL }}/{{ article.prev_page_in_subcategory2.url}}">
                {{ article.prev_page_in_subcategory2.title }}
            </a>
        </li>
    {% endif %}
    {% if article.next_page_in_subcategory2 %}
        <li>
            <a href="{{ SITEURL }}/{{ article.next_page_in_subcategory2.url}}">
                {{ article.next_page_in_subcategory2.title }}
            </a>
        </li>
    {% endif %}
    </ul>

